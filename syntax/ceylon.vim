" Vim syntax file
" Language:    Ceylon (http://ceylon-lang.org/)
" Maintainer:  Artur Grunau <artur.grunau@uj.edu.pl>
" URL:         https://bitbucket.org/amgaera/vim-ceylon-syntax
" Last Change: 2012 July 01

if version < 600
  syntax clear
elseif exists("b:current_syntax")
  finish
endif

syn case match

" Imports
syn keyword	ceylonImport		import

" Keywords
syn keyword	ceylonKeyword		class extends interface satisfies exists is
syn keyword	ceylonKeyword		return value
syn keyword	ceylonConditional	if else
syn keyword	ceylonRepeat		while for in break continue

" Annotations
syn keyword	ceylonAnnotation	shared variable abstract formal default actual
syn keyword	ceylonAnnotation	doc by

" Types
syn keyword	ceylonType			void Integer Object Boolean String

" Functions
syn keyword	ceylonFunction		print

" Numeric literals
syn match	ceylonInteger		"\<\(\d\+\|\d\{1,3}\(_\d\{3}\)\+\)[kMGTP]\?\>"

" String literals
syn region	ceylonString		start=+"+ end=+"+

" Constants
syn keyword	ceylonConstant		null false true

" Comments
syn region	ceylonComment		start="/\*" end="\*/"
syn match	ceylonLineComment	"\(//.*\|\#!.*\)"

" Folds
syn region ceylonFold start="{" end="}" transparent fold

" Map Ceylon groups to standard groups
hi link ceylonImport		Include
hi link ceylonKeyword		Keyword
hi link ceylonConditional	Conditional
hi link ceylonRepeat		Repeat
hi link ceylonAnnotation	Comment
hi link ceylonType			Type
hi link ceylonFunction		Function
hi link ceylonInteger		Number
hi link ceylonString		String
hi link ceylonConstant		Constant
hi link ceylonComment		Comment
hi link ceylonLineComment	Comment

let b:current_syntax = "ceylon"
