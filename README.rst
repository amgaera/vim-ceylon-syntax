About
=====

``vim-ceylon-syntax`` provides Vim syntax highlighting and file type
detection for the Ceylon language.

To learn about Ceylon visit: http://ceylon-lang.org/

Usage
=====

To make Ceylon syntax highlighting available and have it load
automatically, copy the provided files to their corresponding
directories in ``~/.vim/``::

	cp syntax/ceylon.vim ~/.vim/syntax/ceylon.vim
	cp ftdetect/ceylon.vim ~/.vim/ftdetect/ceylon.vim
